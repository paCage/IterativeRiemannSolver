MODULE_NAME := ApproxRiemannSolver

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := p_star_init_guess.o \
        star_region_iterative.o \


TEST_OBJS := p_star_init_guess_tests.o \
             star_region_iterative_tests.o \


LIBS :=
paCages := HydroBase IdealGas RiemannProblem


include ./Makefile.paCage/Makefile
