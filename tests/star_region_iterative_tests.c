/*
 * star_region_iterative_tests.c
 */


#include <cgreen/cgreen.h>
#include <math.h>
#include "./../src/star_region_iterative.h"
#include "./Falcinelli-2012-test-cases.h"
#include "./sod_s_test.h"
#include "./blast_waves_test.h"
#include "./einfeldt_test.h"
#include "./two_shocks_test.h"


#define RHO (1.23)
#define VX (2.34)
#define P (3.45)
#define PRES (0.05) /* 5 percent */


Describe(star_region_iterative);
BeforeEach(star_region_iterative) {};
AfterEach(star_region_iterative) {};

hydro_t q1 = {.rho = RHO, .u = VX, .p = P};
hydro_t q2 = {.rho = RHO, .u = VX, .p = 2 * P};


Ensure(star_region_iterative, passes_Falcinelli_s_tests)
{
  rp_star_region_t star;

  for (int i = 0; i < 7; i++)
  {
    star_region_iterative(&fal_left[i], &fal_right[i],
        fal_v_idx, fal_right[i].p, 100, &star);

    assert_that_double(
        fabs(fal_pstar_pr - (star.p / fal_right[i].p)),
        is_less_than_double(fal_pstar_pr * PRES));
  }
}


Ensure(star_region_iterative, passes_Sod_s_test)
{
  rp_star_region_t star;

  star_region_iterative(&sod_left, &sod_right, vx_h, sod_right.p,
      100, &star);

  assert_that_double(
      fabs(star.p - sod_star.p),
      is_less_than_double(sod_star.p * PRES));

  assert_that_double(
      fabs(star.u - sod_star.u),
      is_less_than_double(sod_star.u * PRES));

  assert_true(star.left.is_shock == sod_star.left.is_shock);
  assert_true(star.right.is_shock == sod_star.right.is_shock);

  assert_that_double(
      fabs(sod_star.left.rarefaction.rho - star.left.rarefaction.rho),
      is_less_than_double(sod_star.left.rarefaction.rho * PRES));

  assert_that_double(
    fabs(sod_star.right.shock.rho - star.right.shock.rho),
      is_less_than_double(sod_star.right.shock.rho * PRES));
}


Ensure(star_region_iterative, passes_blast_waves_test)
{
  rp_star_region_t star;

  for (int i = 0; i < 2; i++)
  {
    star_region_iterative(&blast_waves_left[i], &blast_waves_right[i],
        vx_h, blast_waves_right[i].p, 100, &star);

    assert_that_double(
        fabs(blast_waves_star[i].p - star.p),
        is_less_than_double(blast_waves_star[i].p * PRES));

    assert_that_double(
        fabs(blast_waves_star[i].u - star.u),
        is_less_than_double(fabs(blast_waves_star[i].u * PRES)));

    assert_true(blast_waves_star[i].left.is_shock);
    assert_true(blast_waves_star[i].right.is_shock);

    assert_that_double(
        fabs(blast_waves_star[i].left.shock.rho - star.left.shock.rho),
        is_less_than_double(blast_waves_star[i].left.shock.rho * PRES));

    assert_that_double(
        fabs(blast_waves_star[i].right.shock.rho - star.right.shock.rho),
        is_less_than_double(blast_waves_star[i].right.shock.rho * PRES));
  }
}


Ensure(star_region_iterative, passes_Einfeldt_test)
{
  rp_star_region_t star;

  star_region_iterative(&einfeldt_left, &einfeldt_right, vx_h,
       einfeldt_right.p, 100, &star);

  assert_that_double(
      fabs(star.p - einfeldt_star.p),
      is_less_than_double(einfeldt_star.p * PRES));

  assert_that_double(
      fabs(star.u - einfeldt_star.u),
      is_less_than_double(einfeldt_star.u * PRES));

  assert_true(star.left.is_shock == einfeldt_star.left.is_shock);
  assert_true(star.right.is_shock == einfeldt_star.right.is_shock);

  assert_that_double(
      fabs(einfeldt_star.left.rarefaction.rho - star.left.rarefaction.rho),
      is_less_than_double(einfeldt_star.left.rarefaction.rho * PRES));

  assert_that_double(
      fabs(einfeldt_star.right.rarefaction.rho - star.right.rarefaction.rho),
      is_less_than_double(einfeldt_star.right.rarefaction.rho * PRES));
}


Ensure(star_region_iterative, passes_two_shocks_test)
{
  rp_star_region_t star;

  star_region_iterative(&two_shocks_left, &two_shocks_right, vx_h,
       two_shocks_right.p, 100, &star);

  assert_that_double(
      fabs(star.p - two_shocks_star.p),
      is_less_than_double(two_shocks_star.p * PRES));

  assert_that_double(
      fabs(star.u - two_shocks_star.u),
      is_less_than_double(two_shocks_star.u * PRES));

  printf("u_* = %f (Expected two shocks u_* = %f)\n", star.u, two_shocks_star.u);

  assert_true(star.left.is_shock == two_shocks_star.left.is_shock);
  assert_true(star.right.is_shock == two_shocks_star.right.is_shock);

  assert_that_double(
      fabs(two_shocks_star.left.shock.rho - star.left.shock.rho),
      is_less_than_double(two_shocks_star.left.shock.rho * PRES));

  assert_that_double(
      fabs(two_shocks_star.right.shock.rho - star.right.shock.rho),
      is_less_than_double(two_shocks_star.right.shock.rho * PRES));
}


Ensure(star_region_iterative, returns_stable_results)
{
  rp_star_region_t star;
  double init_guess = 4 * P / 3;

  star_region_iterative(&q1, &q2, vx_h, init_guess, 100, &star);
  double p_star_0 = star.p;

  star_region_iterative(&q1, &q2, vx_h, init_guess * .1, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * .2, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * .5, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * .9, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * 1.1, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * 1.5, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * 1.8, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));

  star_region_iterative(&q1, &q2, vx_h, init_guess * 2.0, 100, &star);
  assert_that_double(star.p, is_equal_to_double(p_star_0));
}
