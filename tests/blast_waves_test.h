#ifndef _BLAST_WAVES_TEST_H_
#define _BLAST_WAVES_TEST_H_

/*
 * Woodward, P., and Colella, P. 1984
 * The pressure and flow resulting from the deposition of a large amount
 * of energy in a small very localised volume.
 * This is a very severe test problem, the left half containing a left 
 * rarefaction, a contact and a right shock, and the right half containing 
 * a left shock, a contact discontinuity and a right rarefaction.
 * */


#include <stdbool.h>
#include <HydroBase.h>
#include <RiemannProblem.h>


hydro_t blast_waves_left[2] = {
  {.rho = 1.0, .p = 1000},
  {.rho = 1.0, .p = 0.01}
};

hydro_t blast_waves_right[2] = {
  {.rho = 1.0, .p = 0.01},
  {.rho = 1.0, .p = 100}
};

rp_star_region_t blast_waves_star[2] = {
  {
    .p = 460.894,
    .u = 19.5975,
    .left.is_shock = true,
    .left.shock.rho = 0.57506,
    .right.is_shock = true,
    .right.shock.rho = 0.26557
  },
  {
    .p = 46.0950,
    .u = -6.19633,
    .left.is_shock = true,
    .left.shock.rho = 5.99242,
    .right.is_shock = true,
    .right.shock.rho = 0.57511
  }
};


#endif /* _BLAST_WAVES_TEST_H_ */
