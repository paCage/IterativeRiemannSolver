#ifndef _STAR_REGION_ITERATIVE_H_
#define _STAR_REGION_ITERATIVE_H_


#include <HydroBase.h>
#include <RiemannProblem.h>


#ifdef __cplusplus
extern "C" {
#endif

#define NITER_DEFAULT (100)


void star_region_iterative(hydro_t *left, hydro_t *right, int v_idx,
    double p_star_0, int niter, rp_star_region_t *star);

#ifdef __cplusplus
}
#endif


#endif /* _STAR_REGION_ITERATIVE_H_ */
