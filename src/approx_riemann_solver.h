#ifndef _APPROX_RIEMANN_SOLVER_H_
#define _APPROX_RIEMANN_SOLVER_H_


#include <HydroBase.h>


#ifdef __cplusplus
extern "C" {
#endif

#define NITER (100)

#ifdef __cplusplus
}
#endif


#endif /* _APPROX_RIEMANN_SOLVER_H_ */
